'use strict'


const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');
const copyfonts = require('gulp-copy');
const clean = require('gulp-clean');

gulp.task('sass', function() {

    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));

});

gulp.task('sass:watch', function() {
gulp.watch('./css/*.scss', gulp.series('sass'));

});

gulp.task('browser-sync', function() {

    var files = ['./*html', './css/*.css', './imagenes/*.{png,jpg,gif,jpeg}', './js/*.js'];

    browserSync.init(files, {
       server: {
            baseDir: './'
        }
    });
});

gulp.task('default', ['browser-sync'], function() {

    gulp.start('sass:watch');

});


gulp.task('clean', function() {

    return del(['dist']);

});


gulp.task('copyfonts', function() {

    gulp.src('./node_modules/open-iconic/font/fonts/*.*')
        .pipe(gulp.dest('dist/imagenes'));

});



gulp.task('imagemin', function() {

    return gulp.src(['./imagenes/*.*', './imagenes/carousel/*.*'])

        .pipe(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('./dist/imagenes'))
});


gulp.task('usemin', function() {

    return gulp.src('./*.html')

        .pipe(usemin({
            css: [rev],
            html: [function() { return htmlmin({ collapseWhitespace: true }); }],
            js: [uglify, rev],
            inlinejs: [uglify],
            inlinecss: [cleanCss, 'concat']
        }))
        .pipe(gulp.dest('dist/'));
});


gulp.task('build', function() {

   gulp.start('copyfonts','imagemin', 'usemin');

});